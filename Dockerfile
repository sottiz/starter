# Permet de compiler le code TS en JS
FROM node:16-alpine AS builder
WORKDIR /app
COPY . /app
# Installe les dépendances
RUN npm install
# Compile le code TS en JS
RUN npm run build

# Permet de lancer le programme
FROM node:16-alpine
WORKDIR /app
# Copie le répertoire /app/dist du premier stage dans le second
COPY --from=builder /app/dist /app/dist
# Copie le fichier package.json du premier stage dans le second
COPY --from=builder /app/package.json /app/package.json
# Installe UNIQUEMENT les dépendances de production (pas les DevDependencies)
RUN npm install --production
# Lance le programme
CMD ["npm", "start"]

# Pour run 
# Option -p (publish) permet de connecter le port 3000 du container au port de la machine
# Le premier port est celui de la machine, le second celui du container
# Exemple: host(3000) -> container(3000)
# L'option -e permet de passer des variables d'environnement au container
# Docker run command: ❯ docker run -p 3000:4000 -e PORT=4000 starter